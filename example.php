<?php
	include_once("management_boekfeeder.class.php");

	$show = isset($_REQUEST['show']) ? $_REQUEST['show'] : "div";
	$q    = isset($_REQUEST['q'])    ? $_REQUEST['q'] : "kaas";
	$n    = isset($_REQUEST['n'])    ? $_REQUEST['n'] : 5;
	$a    = isset($_REQUEST['a'])    ? $_REQUEST['a'] : 0;
	//print_r( ManagementboekFeeder::assoc( $q ,$a,$n) );
	//print_r( ManagementboekFeeder::get_books( array( 'nieuw' => 'Non-profit', 'num' => 20 ) ) );
	//print_r( ManagementboekFeeder::get_rubrieken() );
	//print_r( ManagementboekFeeder::get_trefwoorden() );
?>

<!DOCTYPE HTML>
<html lang="nl-NL">
<head>
	<meta charset="UTF-8">

	<title>Feedreader voor Managementboek.nl</title>

	<style>
		body{font-family:Verdana, Tahoma, Arial, Helvetica,sans-serif;font-size:11px;background-color:#f5f5f5;margin:0}
	</style>
</head>
<body>
	<form action="<?=$_SERVER['PHP_SELF']?>">
		<label>Type
			<select name="show">
				<option<?php if($show == "div") echo " selected=\"selected\"";?>>div</option>
				<option<?php if($show == "table") echo " selected=\"selected\"";?>>table</option>
			</select>
		</label>
		<label>Zoek<input type="text" name="q" value="<?=$q?>"/> </label>
		<label>Affiliate<input type="text" name="a" value="<?=$a?>"/> </label>
		<label>Aantal resultaten<input type="text" name="n" value="<?=$n?>"/> </label>
		<input type="submit"/>
	</form>

		<?php
			if( isset($show) )
			{
				switch ($show)
				{
					case "div":
						echo ManagementboekFeeder::draw_bookdiv( $q ,$a,$n);
						break;
					case "table":
						echo ManagementboekFeeder::draw_booktable( $q ,$a,$n);
						break;
				}
			}
		?>
</body>
</html>
