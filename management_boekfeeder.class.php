<?php

	/**
	 * Class ManagementboekFeeder
	 *
	 * Haal gegevens op van Managementboek.nl met behulp van de xmlfeed
	 * Deze class maakt gebruik van de standaard php xmp_parser ( http://php.net/manual/en/ref.xml.php )
	 *
	 * Eenieder kan op zijn/haar website de actuele informatie van beschikbare boeken op https://managementboek.nl tonen
	 * Om inkomsten te genereren uit verkopen die vanuit uw website komen, kunt u zich aanmelden
	 * op https://www.managementboek.nl/uwaccount/affiliate_overzicht voor het partnerprogramma
	 *
	 * Deze class bevat een drietal manieren om op een heel simpele manier de gegevens op te halen van een of meerdere boeken.
	 * Daarnaast bevat het functies om de beschikbare rubrieken en trefwoorden op te vragen
	 * U kunt een zoekterm opgeven (deze kan uiteraard ook een isbn/ean bevatten), uw affiliatenummer en het aantal te tonen resultaten
	 *
	 * De functies assoc, draw_boekdiv en draw_booktable verwachten niet meer dan drie simpele variabelen om tot resultaat te komen.
	 * De functie get_books verwacht een array met opties en geeft bijvoorbeeld ook de mogelijkheid om op rubriek of trefwoord te zoeken.
	 * De resultaten van deze functie (een associatieve array) kunnen gebruikt worden om een pagina te vullen
	 *
	 * @example
	 * geef een associatieve array met 15 boeken die gevonden worden op zoekterm 'pratchett', gebruik affiliate nummer 1668 in de links
	 * $boeken = ManagementboekFeeder::assoc("pratchett",1668,15);
	 * of
	 * $boeken = ManagementboekFeeder::get_books( array( "q" => "pratchett", "affiliate" => 1668, "num" => 15);
	 *
	 * @example
	 * Toon een div met 15 boeken die gevonden worden op zoekterm 'pratchett', gebruik affiliate nummer 1668 in de links
	 * echo ManagementboekFeeder::draw_bookdiv("pratchett",1668,15);
	 *
	 * @example
	 * Toon een tabel met 15 boeken die gevonden worden op zoekterm 'pratchett', gebruik affiliate nummer 1668 in de links
	 * echo ManagementboekFeeder::draw_booktable("pratchett",1668,15);
	 *
	 * @example
	 * Toon een tabel met de informatie van "Bowie - de biografie"
	 * echo ManagementboekFeeder::draw_booktable("9789492037312",1668);
	 *
	 *
	 * @example
	 * geef een array met beschikbare rubrieken
	 * $rubrieken = ManagementboekFeeder::get_rubrieken()
	 *
	 * @example
	 * geef een array met beschikbare trefwoorden
	 * $rubrieken = ManagementboekFeeder::get_trefwoorden()
	 *
	 */

	class ManagementboekFeeder
	{
		/**
		 * @param string $q zoekterm
		 * @param int $affiliate affiliatenummer
		 * @param int $num aantal resultaten
		 * @return array
		 */
		public static function assoc( $q, $affiliate = 0, $num = 10 )
		{
			$options['q']         = $q;
			$options['affiliate'] = $affiliate;
			$options['num']       = $num;
			$boeken = self::get_books( $options );
			return $boeken;
		}

		/**
		 * @param string $q zoekterm
		 * @param int $affiliate
		 * @param int $num
		 * @return string html met de resultaten in divs
		 */
		public static function draw_bookdiv( $q, $affiliate = 0, $num = 10 )
		{
			$options['q']         = $q;
			$options['affiliate'] = $affiliate;
			$options['num']       = $num;
			$boeken = self::get_books( $options );

			$html = "<div id=\"managementboek_boeken\">";
			foreach($boeken as $boek)
			{
				$html .= "<div class=\"boek\">\n";
				$html .= "  <div class=\"cover\"><img src=\"{$boek['img']}\" alt=\"{$boek['ean']}\"/></div>\n";
				$html .= "  <div class=\"titel\">\n";
				$html .= "    <a href=\"{$boek['url']}\">\n";
				$html .= "      <span class=\"boektitel\">{$boek['titel']}</span>\n";
				$html .= "      <span class=\"ondertitel\">{$boek['ondertitel']}</span>\n";
				$html .= "      <span class=\"uitgever\">{$boek['uitgever']}</span>\n";
				$html .= "    </a>\n";
				$html .= "  </div>\n";
				$html .= "  <div class=\"auteurs\">\n";
				if( is_array($boek['auteur']) )
				{
					foreach( $boek['auteur'] as $auteur )
					{
						if( ! is_array($auteur) ) continue;
						$html .= "    <div class=\"auteur\">\n";
						$html .= "      <span><a href=\"{$auteur['url']}\">{$auteur['naam']}</a></span>\n";
						$html .= "    </div>\n";

					}
				}
				$html .= "  </div>\n";
				$html .= "  <div class=\"meta\">\n";
				$html .= "      <span class=\"verschijningsdatum\">{$boek['verschijningsdatum']}</span>\n";
				$html .= "      <span class=\"prijs\">&euro; ".str_replace(".",",",$boek['europrijs'])."</span>\n";
				$html .= "      <span class=\"bindwijze\">{$boek['bindwijze']}</span>\n";
				$html .= "      <span class=\"taavermelding\">{$boek['taalvermelding']}</span>\n";
				$html .= "      <span class=\"ean\">{$boek['ean']}</span>\n";
				$html .= "      <span class=\"leverbaarheid\">{$boek['leverbaarheid']}</span>\n";
				$html .= "      <span class=\"rubriek\">{$boek['rubriek']}</span>\n";
				$html .= "  </div>\n";
				$html .= "  <div class=\"abstract\">{$boek['abstract']}</span>\n";
				$html .= "  <div class=\"samenvatting\">{$boek['samenvatting']}</span>\n";
				$html .= "  <div class=\"bestel\"><a href=\"{$boek['bestel']}\">Bestel</a></span>\n";
				$html .= "</div>\n";
				$html .= "<div class=\"clear\"></div>\n";
			}
			$html .= "<div>";

			return $html;
		}

		/**
		 * @param string $q zoekterm
		 * @param int $affiliate
		 * @param int $num
		 * @return string html met de resultaten in een tabel
		 */
		public static function draw_booktable( $q, $affiliate = 0, $num = 10 )
		{
			$options['q']         = $q;
			$options['affiliate'] = $affiliate;
			$options['num']       = $num;
			$boeken = self::get_books( $options );


			$html = "<table id=\"managementboek_boeken\">";
			$html .= " <tbody>\n";
			foreach($boeken as $boek)
			{
				$rowcount = 11;
				if (strlen($boek['rubriek']) ) $rowcount++;
				if (strlen($boek['abstract']) ) $rowcount++;
				if (strlen($boek['samenvatting']) ) $rowcount++;
				$html .= "  <tr><td rowspan=\"{$rowcount}\" class=\"cover\"><img src=\"{$boek['img']}\" alt=\"{$boek['ean']}\"/></td></tr>\n";
				$html .= "  <tr>\n";
				$html .= "    <td class=\"titel\">\n";
				$html .= "      <a href=\"{$boek['url']}\">\n";
				$html .= "        <span class=\"boektitel\">{$boek['titel']}</span>\n";
				$html .= "        <span class=\"ondertitel\">{$boek['ondertitel']}</span>\n";
				$html .= "      </a>\n";
				$html .= "    </td>\n";
				$html .= "  </tr>\n";

				$html .= "  <tr><td class=\"uitgever\">{$boek['uitgever']}</td></tr>\n";

				$html .= "  <tr>\n";
				$html .= "    <td class=\"auteurs\">\n";
				if( is_array($boek['auteur']) )
				{
					$auteurs = array();
					foreach( $boek['auteur'] as $i=>$auteur )
					{
						$auteurs[$i] .= "      <span class=\"auteur\"><a href=\"{$auteur['url']}\">{$auteur['naam']}</a></span>\n";
					}
					$html .= implode(", ",$auteurs);
				}
				$html .= "    </td>\n";
				$html .= "  </tr>\n";

				$html .= "  <tr><td class=\"verschijningsdatum\">{$boek['verschijningsdatum']}</td></tr>\n";
				$html .= "  <tr><td class=\"prijs\">&euro; ".str_replace(".",",",$boek['europrijs'])."</td></tr>\n";
				$html .= "  <tr><td class=\"bindwijze\">{$boek['bindwijze']}</td></tr>\n";
				$html .= "  <tr><td class=\"taavermelding\">{$boek['taalvermelding']}</td></tr>\n";
				$html .= "  <tr><td class=\"ean\">{$boek['ean']}</td></tr>\n";
				$html .= "  <tr><td class=\"leverbaarheid\">{$boek['leverbaarheid']}</td></tr>\n";
				if( strlen( $boek['rubriek']) > 0 )
					$html .= "  <tr><td class=\"rubriek\">{$boek['rubriek']}</td></tr>\n";
				if( strlen( $boek['abstract']) > 0 )
					$html .= "  <tr><td class=\"abstract\">{$boek['abstract']}</td></tr>\n";
				if( strlen( $boek['samenvatting']) > 0 )
					$html .= "  <tr><td class=\"samenvatting\">{$boek['samenvatting']}</td></tr>\n";
				$html .= "  <tr><td class=\"bestel\"><a href=\"{$boek['bestel']}\">Bestel</a></td></tr>\n";
			}
			$html .= "  </tbody>";
			$html .= "</table>";

			return $html;
		}

		/**
		 * @param array $options
		 *   array (
		 *    'sort'      sorteersleutel
		 *    'desc'      volgorde sorteersleutel
		 *    'taal'      gewenste taal
		 *    'rubriek'   rubriek
		 *    'trefwoord' trefwoord
		 *    'affiliate' affiliatenummer
		 *    'num'       aantal resultaten
		 *    'nieuw'     toon nieuwe boeken
		 *    'q'         zoekterm
		 *   }
		 * @return array
		 * @throws Exception
		 */
		public static function get_books( $options )
		{
			$skip   = array('perrij', 'toontitel', 'buttonmeer');
			$retval = array();
			if ( ! is_array($options) )
			{
				$options = array(
					'sort' => '30d',
					'desc' => 0,
					'taal' => 'nl',
					'rubriek' => '',
					'trefwoord' => '',
					'affiliate' => 0,
					'num' => 10,
					'nieuw' => '',
					'q' => ''
				);
			}

			$options['sort'] = ! empty($options['sort']) ? (int)$options['sort'] : '30d';
			$options['num']  = (int)$options['num'] > 0 ? (int)$options['num'] : 10;

			if( ! empty($options['rubriek']) )
			{
				$rubrieken = self::get_rubrieken();
				if( ! in_array( $options['rubriek'], $rubrieken ) )
					unset( $options['rubriek'] );
			}

			// skip options we don't need
			foreach ($skip as $s)
			{
				unset($options[$s]);
			}
			$url = "https://www.managementboek.nl/code/affiliates/feed.php?";
			$qs = http_build_query($options, '', '&');

			$data = file_get_contents( $url.$qs );

			$array = self::xml2array( $data );

			if ( $array['resultaat']['boek']['titel'])
				$array = array($array['resultaat']['boek']);
			else
				$array = $array['resultaat']['boek'];

			foreach( $array as $i=>$val)
			{
				$retval[$i] = $val;
				if( ! empty($val['auteur']['naam']) )
				{
					$retval[$i]['auteur'][0]['naam'] = $val['auteur']['naam'];
					$retval[$i]['auteur'][0]['url']  = $val['auteur']['url'];
				}
				unset($retval[$i]['auteur']['naam']);
				unset($retval[$i]['auteur']['url']);
				unset($retval[$i]['auteur']['foto']);

				if( is_array($retval[$i]['ondertitel']) )
					$retval[$i]['ondertitel'] = $retval[$i]['ondertitel'][0];
				if( is_array($retval[$i]['abstract']) )
					$retval[$i]['abstract'] = $retval[$i]['abstract'][0];
				if( is_array($retval[$i]['samenvatting']) )
					$retval[$i]['samenvatting'] = $retval[$i]['samenvatting'][0];
				if( is_array($retval[$i]['rubriek']) )
					$retval[$i]['rubriek'] = $retval[$i]['rubriek'][0];
			}

			return $retval;
		}

		public static function get_rubrieken()
		{
			$url = "https://www.managementboek.nl/code/affiliates/rubrieken.php";

			$data = file_get_contents( $url );

			$array = self::xml2array( $data );
			foreach($array['rubrieken']['rubriek'] as $a)
			{
				$retval[] = $a;
			}
			return $retval;
		}

		/**
		 * @return array
		 * @throws Exception
		 */
		public static function get_trefwoorden()
		{
			$url = "https://www.managementboek.nl/code/affiliates/trefwoorden.php";

			$data = file_get_contents( $url );

			$array = self::xml2array( $data );
			foreach($array['trefwoorden']['trefwoord'] as $a)
			{
				$retval[] = $a;
			}
			return $retval;
		}
		/**
		 * convert the given XML text to an array in the XML structure.
		 *
		 * @param string $contents       - The XML text
		 * @param int    $get_attributes - 1 or 0. If this is 1 the function will get the attributes as well as the tag values - this results in a different array structure in the return value.
		 * @param string $priority       - Can be 'tag' or 'attribute'. This will change the way the resulting array sturcture. For 'tag', the tags are given more importance.
		 *
		 * @return array|void            The parsed XML in an array form. Use print_r() to see the resulting array structure.
		 * @throws Exception
		 * @example                      xml2array(file_get_contents('feed.xml', 1, 'attribute'));
		 * @example                      $array =  xml2array(file_get_contents('feed.xml'));
		 */
		private static function xml2array($contents, $get_attributes = 1, $priority = 'tag')
		{
			if (!$contents) return array();

			//Get the XML parser of PHP - PHP must have this module for the parser to work
			if (!function_exists('xml_parser_create'))
			{
				return array( "msg" =>  "'xml_parser_create()' function not found!");
			}

			$parser = xml_parser_create('');
			xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
			xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
			xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
			if (!xml_parse_into_struct($parser, trim($contents), $xml_values))
			{
				throw new Exception("ongeldige XML in dit bestand");
			}
			xml_parser_free($parser);

			if ( ! $xml_values) return array();

			//Initializations
			$xml_array   = array();
			$parents     = array();
			$opened_tags = array();
			$arr         = array();

			$current     = &$xml_array;

			//Go through the tags.
			$repeated_tag_index = array();//Multiple tags with same name will be turned into an array
			foreach ($xml_values as $data)
			{
				unset($attributes, $value);//Remove existing values, or there will be trouble

				//This command will extract these variables into the foreach scope
				// tag(string), type(string), level(int), attributes(array).
				extract($data);//We could use the array by itself, but this cooler.

				$result = array();
				$attributes_data = array();

				if (isset($value))
				{
					if ($priority == 'tag') $result = $value;
					else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
				}

				//Set the attributes too.
				if (isset($attributes) and $get_attributes)
				{
					foreach ($attributes as $attr => $val)
					{
						if ($priority == 'tag') $attributes_data[$attr] = $val;
						else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
					}
				}

				//See tag status and do the needed.
				if ($type == "open")
				{//The starting of the tag '<tag>'
					$parent[$level - 1] = &$current;
					if (!is_array($current) or (!in_array($tag, array_keys($current))))
					{ //Insert New tag
						$current[$tag] = $result;
						if ($attributes_data) $current[$tag . '_attr'] = $attributes_data;
						$repeated_tag_index[$tag . '_' . $level] = 1;

						$current = &$current[$tag];

					}
					else
					{ //There was another element with the same tag name

						if (isset($current[$tag][0]))
						{//If there is a 0th element it is already an array
							$current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
							$repeated_tag_index[$tag . '_' . $level]++;
						}
						else
						{//This section will make the value an array if multiple tags with the same name appear together
							$current[$tag] = array($current[$tag], $result);//This will combine the existing item and the new item together to make an array
							$repeated_tag_index[$tag . '_' . $level] = 2;

							if (isset($current[$tag . '_attr']))
							{ //The attribute of the last(0th) tag must be moved as well
								$current[$tag]['0_attr'] = $current[$tag . '_attr'];
								unset($current[$tag . '_attr']);
							}

						}
						$last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
						$current = &$current[$tag][$last_item_index];
					}

				}
				elseif ($type == "complete")
				{ //Tags that ends in 1 line '<tag />'
					//See if the key is already taken.
					if (!isset($current[$tag]))
					{ //New Key
						$current[$tag] = $result;
						$repeated_tag_index[$tag . '_' . $level] = 1;
						if ($priority == 'tag' and $attributes_data) $current[$tag . '_attr'] = $attributes_data;
					}
					else
					{ //If taken, put all things inside a list(array)
						if (isset($current[$tag][0]) and is_array($current[$tag]))
						{//If it is already an array...

							// ...push the new element into that array.
							$current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;

							if ($priority == 'tag' and $get_attributes and $attributes_data)
							{
								$current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
							}
							$repeated_tag_index[$tag . '_' . $level]++;

						}
						else
						{ //If it is not an array...
							$current[$tag] = array($current[$tag], $result); //...Make it an array using using the existing value and the new value
							$repeated_tag_index[$tag . '_' . $level] = 1;
							if ($priority == 'tag' and $get_attributes)
							{
								if (isset($current[$tag . '_attr']))
								{ //The attribute of the last(0th) tag must be moved as well

									$current[$tag]['0_attr'] = $current[$tag . '_attr'];
									unset($current[$tag . '_attr']);
								}

								if ($attributes_data)
								{
									$current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
								}
							}
							$repeated_tag_index[$tag . '_' . $level]++; //0 and 1 index is already taken
						}
					}

				}
				elseif ($type == 'close')
				{ //End of tag '</tag>'
					$current = &$parent[$level - 1];
				}
			}

			return ($xml_array);
		}
	}